############################################################
# Dockerfile to container for pandoc based on Ubuntu
############################################################

# Set the base image to Ubuntu
FROM ubuntu

ENV TERM xterm

# File Author / Maintainer
MAINTAINER Steve Angelovich 

# Update the repository sources list
RUN apt-get update -y \
  && apt-get install -y --no-install-recommends \
    texlive-latex-base \
    texlive-xetex latex-xcolor \
    texlive-math-extra \
    texlive-latex-extra \
    texlive-fonts-extra \
    texlive-bibtex-extra \
    fontconfig \
    wget \
    lmodern \
    libgmp10 \
    python-pip

# Install bottle
RUN pip install bottle

# Install the current stable version of pandoc
RUN wget -nc --no-check-certificate http://github.com/jgm/pandoc/releases/download/1.15.1/pandoc-1.15.1-1-amd64.deb
RUN dpkg -i pandoc-1.15.1-1-amd64.deb

# Create the default data directory
RUN mkdir -p /data/work
WORKDIR /data

# Copy in the python source for the server
COPY index.html index.html
COPY index.md index.md
COPY pandoc_client.sh pandoc_client.sh
COPY pandoc_server.cfg pandoc_server.cfg
COPY pandoc_server.py pandoc_server.py

# Start the server
CMD python pandoc_server.py