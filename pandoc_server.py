import os, sys, ConfigParser, shutil
import tarfile, zipfile
from subprocess import call,check_output, CalledProcessError, STDOUT
from bottle import route, run, template, request, static_file

#load a configuration file so we have a working directory
config = ConfigParser.RawConfigParser()
config.read('pandoc_server.cfg')
workdir = config.get('Defaults', 'workdir')
print "workdir=" + workdir
if(workdir == ""):
    raise IOError('Error: workdir must be defined in configuration file, pandoc_server.cfg')
port = config.getint('Defaults', 'port')
if(port == 0):
    raise IOError('Error: port must be defined in configuration file, pandoc_server.cfg')

@route('/')
def index():
    return static_file('index.html', root='.')

@route('/script')
def get_script():
    return static_file('pandoc_client.sh', root='.', download='pandoc_client.sh')

@route('/list')
def do_list():
    paths = []
    for root, dirs, files in os.walk(workdir, topdown=False):
      print ('root: ' + root)
      for name in files:
          fname = os.path.join(root, name)
          fname = os.path.relpath(fname, workdir) ;
          paths.append(fname) ;
      for name in dirs:
          fname = os.path.join(root, name)
          fname = os.path.relpath(fname, workdir) ;
          paths.append(fname) ;

    return { "success" : True, "documents" : paths }

@route('/clean')
def do_clean():
    paths = []
    ls = os.listdir(workdir)
    for entry in ls:
      fname = build_path(entry)
      print build_path(entry)
      if(os.path.isfile(fname)):
        os.remove(fname)
      elif(os.path.isdir(fname)):
        shutil.rmtree(fname)

      paths.append(build_path(entry))

    return { "success" : True, "documents" : paths }

@route('/documents/<thepath:path>', method='GET')
def document_get( thepath ):
    print ('name: ' + thepath)
    name = os.path.basename(thepath)
    return static_file(thepath, root=workdir, download=name)

@route('/documents/<name>', method='DELETE' )
def document_delete( name ):
    fname = build_path(name)
    if(os.path.isfile(fname)):
       os.remove(fname)
    elif(os.path.isdir(fname)):
       shutil.rmtree(fname)

    return "DELETE " + name

@route('/convert', method='GET' )
def convert():
    return '''
	<form action="/convert" method="post">
	  Pandoc options: <input type="text" name="pandoc_args" value="-s --latex-engine=xelatex"/>
	  Input format: <input type="text" name="input_file"/>
	  Output format: <input type="text" name="output_file"/>
	  <input type="submit" value="Convert" />
	</form>
   '''

@route('/convert', method='POST' )
def do_convert():
    options = request.forms.get('pandoc_args')
    ifile = request.forms.get('input_file')
    ofile = request.forms.get('output_file')
    print 'file: ' + ifile
    print 'ofile: ' + ofile
    print 'options: ' + options
    do_pandoc(build_path(ifile), options, build_path(ofile))
    return 'OK'

@route('/upload')
def upload():
   return '''
	<form action="/upload" method="post" enctype="multipart/form-data">
	  Select a file: <input type="file" name="filename" />
	  <input type="submit" value="Upload" />
	</form>
   '''

@route('/upload', method='POST')
def do_upload():
    input_file = request.forms.get('input_file')
    upload = request.files.get('filename')
    upload.save(workdir, overwrite='true')
    #if it an archive we understand extract it
    extract_file(upload.filename, workdir)
    return 'OK'

def do_pandoc(inFile, options, outFile):

    to_directory = os.path.dirname(inFile)
    cwd = os.getcwd()
    os.chdir(to_directory)

    fnameIn = os.path.basename(inFile)
    fnameOut = os.path.basename(outFile)

    cmd = 'pandoc' + ' ' + options + ' ' + fnameIn + ' -o ' + fnameOut
    print 'cmd=' + cmd 
    try:
      cmd_output = check_output(cmd, stderr=STDOUT, shell=True, universal_newlines=True)
    except CalledProcessError as e:
      err = 'command=', cmd, 'exit code=', e.returncode, 'stdout/stderr=', e.output
      raise RuntimeError(err)

    os.chdir(cwd)
    return

def build_path(basename):
    return os.path.join(workdir, basename)

def extract_file(path, to_directory):
 if path.endswith('.zip') or path.endswith('tar'):
    opener, mode = zipfile.ZipFile, 'r'
 elif path.endswith('.tar.gz') or path.endswith('.tgz'):
     opener, mode = tarfile.open, 'r:gz'
 elif path.endswith('.tar.bz2') or path.endswith('.tbz'):
     opener, mode = tarfile.open, 'r:bz2'
 else:
     return

 cwd = os.getcwd()
 os.chdir(to_directory)

 try:
     file = opener(path, mode)
     try: file.extractall()
     finally: file.close()
 finally:
     os.chdir(cwd)


def main(argv):
   run(host='0.0.0.0', port=port, debug=True)

if __name__ == "__main__":
   main(sys.argv[1:])



