#Index

This server exposes a small RESTful interface that uses pandoc to convert documents from one
format to another.  The primary motivation for this server was to learn about Docker,
create a RESTful API and to be able to convert markdown documents to PDF's without
having to go through the brain-damage of installing all the required dependencies.

This implementation uses python Bottle to expose the RESTful API but is not designed 
for high performance or concurrent operations.

Usage of the server would typically be something like; upload markdown file, convert to pdf, download pdf 
version of the document, delete the markdown and pdf file.  This can be done via a browser or more typically
using curl wrapped in bash script to do all the grunt work.

##The server supports the following operations

###Index.html

 * command = http://localhost:8080/
 * verb = GET
 * Response = Returns html document index.html which contains the basic information about the interface.

###Upload

* command = http://localhost:8080/upload
* verb = GET
* Returns the form used to upload a file

###Upload

* command = http://localhost:8080/upload
* verb = POST
* Uploads a file and stores it on the server. If the file is a tar or zip file then the contents are expanded on upload.

###Convert 

* command = http://localhost:8080/convert
* verb = GET
* Returns the form used to convert a file

###Convert

* command = http://localhost:8080/convert
* verb = POST
* Converts from the input to output formats using the specified pandoc arguments.  The resulting document
is left on the file system in the same folder as the input and can be retrieved using the download command.

###Download Document
* command = http://localhost:8080/documents/\<name\>
* verb = GET
* Returns the requested file

###Delete Document

* command = http://localhost:8080/documents/\<name\>
* verb = DELETE
* Deletes the specified file or directory

###List 

* command = http://localhost:8080/list
* verb = GET
* Returns list of files and directories in the severs work folder

###Cleanup

* command = http://localhost:8080/clean
* verb = PUT
* Deletes all files and directories in the servers work folder

###Script

* command = http://localhost:8080/script
* verb = GET
* Downloads a bash script that wraps the curl commands to interact with he server
