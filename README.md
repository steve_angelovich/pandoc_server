# README #

### Summary ###

This repository contains the source to generate a Docker container that provides a RESTful API to facilitate the conversion of documents using the pandoc utility.

### How do I get set up? ###

* Install Docker
* Clone this repository
* The script build.sh will download the base image and all the required dependencies to create the pandoc_server container.
* The script start.sh will run this docker container and start listening on port 8080 by default.
* Browse to http://localhost:8080/ to see an overview of the interface.
* A client side bash script can be downloaded at http://localhost:8080/script which can be used to drive the document conversion server via a script.  The script wraps a number of curl commands.
