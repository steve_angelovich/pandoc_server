#!/bin/bash

#This script wraps the curl commands so a client can interact with the server without having to worry about
#the details of the curl command.

hp=localhost:8080
pandoc_args="-s --latex-engine=xelatex"

TEMP=`getopt -o h --long help,clean,index,list,upload:,input:,output:,convert,download:,delete:,pandoc_args, -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"
input=""
output=""
convert=false
while true; do
  case "$1" in
    -h | --help )
        echo "--help"
        shift
        firefox http://$hp/
        exit
       ;;
     --upload )
        shift
        echo "-u  $1"
        curl -k -i -H "Content-Type=multipart/form-data" -F "filename=@$1;type=text/xml" http://$hp/upload
        result=$?
        echo
        exit $result
       ;;
    --clean )
        shift
        curl -X GET http://$hp/clean
        result=$?
        echo
        exit $result
       ;;
    --list )
        shift
        curl -X GET http://$hp/list
        result=$?
        echo
        exit $result
       ;;
   --download )
        shift
        filename=$1
        shift
        fname=$(basename $filename)
        echo "--download  $filename to $fname"
        curl -X GET http://$hp/documents/$filename > $fname
        result=$?
        echo
        exit $result
       ;;
    --delete )
        shift
        filename=$1
        echo "--delete  $filename"
        shift
        curl -X DELETE http://$hp/documents/$filename
        result=$?
        echo
        exit $result
       ;;
    --input )
        shift
        input=$1
        shift
        echo "--input $input"
       ;;
    --output )
        shift
        output=$1
        shift
        echo "--output $output"
       ;;
    --convert )
        shift
        echo "--convert"
        convert=true
       ;;
     --pandoc_args )
        shift
        pandoc_args=$1
        shift
        echo "--pandoc_args"
       ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if [ $convert ] ; then
   echo "--convert --input=$input --output=$output"
   echo "--pandoc_args=$pandoc_args"
   curl -X POST -F "pandoc_args=$pandoc_args" -F "input_file=$input" -F "output_file=$output" http://$hp/convert
   result=$?
   echo
   exit $result
fi

echo "pandoc_client.sh examples"
echo " pandoc_client.sh --help displays the index.html from the server with basic information on the server"
echo " pandoc_client.sh --upload <filename> uploads a file to the server in preparation for converting it to some other format"
echo " pandoc_client.sh --list returns a list of files on the server that could be converted"
echo " pandoc_client.sh --input <inputfile> --output <outputfile> --convert, converts the input file to the output filename and format"
echo " pandoc_client.sh --download <filename> download the file placing it in the current directory"
echo " pandoc_client.sh --delete <filename> deletes the file/directory from the server"
echo " pandoc_client.sh --clean Deletes all the files/directories from the server"
exit 1
